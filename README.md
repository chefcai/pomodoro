# How to do what you should do

Let me preface this by saying I am an extremely focused and studious person. 
Where that focus starts, stays, and ends are difficult to manage.
I could be a poster child distraction management. 

It's been over a decade since Merlin Mann, productivity revolutionary, Inbox Zero went viral and is still seen as (perhaps) unattainable:

### Inbox Zero
[Inbox Zero](http://bit.ly/caiinbox0 "YouTube Video")(YouTube)

This fabled land in which my day is email free and focus on whatever actionable task I have at hand. 
Or, in a lot of cases, free to roam the internet and explore other distracting opportunities.
I'm not talking about actively disengaging from work, catching up on the latest binge-worthy video release, just work-related distractions - and anyone can benefit from this.

There are many distractions which may live in our world. 
Even well-intentioned and interrupting co-workers, Meetings, Games, Websites, Social Media sites, Background Noise amongst many others.
Have you ever started exploring new technology, blog post, and "just one more" video/article/social media click?

I have.

Before I know it, I've dropped into the k-hole of distraction with nothing to break my impenetrable focus. 
The end of my day comes, and realization strikes: I haven't accomplished anything or not nearly as much as I am capable of.

### Solutions?

* Schedule everything: As with Inbox Zero this method seems unsustainable - it might work well if I'm not responding to time-sensitive requests, have crises that develop, or a co-worker blowing up my world.
* Task Apps
* Browser blocking plug-ins which are available across many platforms
* "Multi-task" (and fail)
But in the end the one I return to:

## Pomodoro

The Pomodoro Technique will work with any other task management system. 
Though, as with most self-accountability methods, you have to buy-in and completely buy-in.

Interval timers are now fairly prolific HIIT stop watches, software, iOS App Store and Google Play.

I prefer a physical timer just as creator Francesco Cirillo chose in the 1908s.

Originally named after a kitchen timer Francesco Cirillo used shaped tomato. The technique breaks up the work day (8 hours) into 16x 25-minute active-work sessions (called a pomodoro), each section with a 5 minute break, and after 4 pomodoros a longer 15-30 minute break. What you do with the break times are up to you: water, coffee, quick chat, bathroom, weed through email, reconcile your current progress and opt to task switch.

For me the 25-minute window is an immovable object. When an interruption arises - I make two choices depending on the interruption: I absorb the remaining time into this interruption if >5 minutes remains, or I ask for 10 minutes before I address what they need.

## Pomodoro Tools
Hardware:

* Interval Timer [Gymboss](http://bit.ly/caigymboss "Amazon Gymboss")
  - https://www.gymboss.com/

 Software: 

* Pomodoro App (MaxOS and Windows) [Tomighty](http://bit.ly/caitmty "Tomighty Website")
  - http://tomighty.com/

### Honorable Mentions:
Task App:

* [Wunderlist](http://bit.ly/caiwndrlst "Wunderlist Website")
  - https://www.wunderlist.com/
* [Google Keep](http://bit.ly/caikeep "Google Keep Website")
  - https://keep.google.com
* Apple Notes 

* [Remember the Milk](http://bit.ly/cairemthemilk "Remember the Milk Website")
  - https://www.rememberthemilk.com/
* [Trello](http://bit.ly/caitrello "Trello Website")
  - https://trello.com/
* [I Done This](http://bit.ly/caiidonethis "idonethis website")
  - https://idonethis.com

Browser blocking plug-ins which are available across many platforms 
Chrome:

* [StayFocusd](http://bit.ly/caistyfcsd "StayFocusd Chrome Plug-in")
  - https://chrome.google.com/webstore/detail/stayfocusd/laankejkbhbdhmipfmgcngdelahlfoji

OS level blocking app:

* MacOS - [Self Control](http://bit.ly/caislfctrl "Self-Control Website))
  - https://selfcontrolapp.com/
* iPhone/Android - [Freedom App](http://bit.ly/caifrdmapp "Freedom Application Website")
  - https://freedom.to/
